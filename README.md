# todoium

Todoium started out as a file with just `touch $HOME/.todo/$1`, but has since
evolved to be much more.

To use Todoium just enter the keyword `todoium` followed by either an option or
the name of the todo you wish to create. All your todos are stored in
a directory, which is `~/.todo` by default. Each todo is an empty file with the
name of the file as the todo. This way you don't need to learn every weird
command to in order just to delete or rename todos, you can just use `rm` and
`mv`.

Here is the list of options that you can use after typing `todoium`:

| Option | Long Option | Description                 |
| ------ | ----------- | --------------------------- |
| -h     | --help      | Show Help/Info              |
| -l     | --list      | List Your Todos             |
| -t     | --tree      | List Your Todos Using Tree  |
| -p     | --path      | Echo The Path of Your Todos |

If you do not specify an option then Todoium assumes that you want to create
a todo in `$tododir` (`~/.todo/` by default). Note that you can not put spaces
in the todo's name (`todoium take out trash`), you have to type `todoium take\
out\ trash` or `todoium "take out trash"`. But I recommend you just you use
hyphens or underscores (`todoium take-out-trash` or `todoium take_out_trash`).

## Installation 

To install Todoium all you need to move the `todoium` file somewhere into
your `$PATH`.

Unless you want to type `todoium` every time you want to write a todo, add
this command to your `.bashrc` or `.zshrc` file:

	alias todo='todoium'

Or for the REALLY lazy people, add this instead:

	alias t='todoium'

Now instead of typing `todoium` you just need to type `todo` or `t`,
depending on which alias you added(how lazy you are).

In order to use the -t option you need to have `tree` installed. For
Debain based Linux distros you can type:

	sudo apt-get install tree

## Contributing

I am open to pull requests just as long as you know [how to to write a commit
message](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).
Please report any bugs/issues you find. I am also very open to new ideas and
critique, see Contact section below on how to contact me.

## Contact

If you have any questions or comments please contact me at edvb54@gmail.com or
leave a comment wherever.

-ED

## Licence

GPL v3 License

